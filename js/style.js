let paragraphs = document.getElementsByTagName("p");
for (let i = 0; i < paragraphs.length; i++) {
paragraphs[i].style.backgroundColor = "#ff0000";
}

let optionsList = document.getElementById("optionsList");
console.log(optionsList);

let parentElement = optionsList.parentElement;
console.log(parentElement);

let childNodes = optionsList.childNodes;

for (let i = 0; i < childNodes.length; i++) {
  let nodeName = childNodes[i].nodeName;
  let nodeType = childNodes[i].nodeType;
  console.log("Node Name: " + nodeName + ", Node Type: " + nodeType);
}

let contentTestParagraph = document.getElementById("testParagraph");
contentTestParagraph.textContent = "This is a paragraph";

let mainHeader = document.querySelector(".main-header");
let nestedElements = mainHeader.querySelectorAll("*");

for (let i = 0; i < nestedElements.length; i++) {
  nestedElements[i].classList.add("nav-item");
}

let sectionTitles = document.querySelectorAll(".section-title");

for (let i = 0; i < sectionTitles.length; i++) {
  sectionTitles[i].classList.remove("section-title");
}